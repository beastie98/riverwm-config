#!/bin/sh
killall -q yambar
while pgrep -x yambar > /dev/null; do sleep 1; done
exec yambar
