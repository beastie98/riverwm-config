#!/bin/sh
killall -q pipewire 
killall -q pipewire-pulse
while pgrep -x pipewire  > /dev/null; do sleep 1; done
while pgrep -x pipewire-pulse  > /dev/null; do sleep 1; done
pipewire &
exec pipewire-pulse
